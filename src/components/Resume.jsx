import React from 'react';

const styles = {
  nameStyle: {
    fontSize: '5em',
  },
  inlineChild: {
    display: 'inline-block',
  },
  mainContainer: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

function Resume() {
  return (
    <div style={styles.mainContainer}>
      TEST
    </div>
  );
}

export default Resume;
